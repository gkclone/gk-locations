<?php
/**
 * @file
 * gk_locations.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function gk_locations_taxonomy_default_vocabularies() {
  return array(
    'facilities' => array(
      'name' => 'Facilities',
      'machine_name' => 'facilities',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
