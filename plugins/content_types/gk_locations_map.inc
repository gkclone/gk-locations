<?php

$plugin = array(
  'title' => 'Locations: Map',
  'category' => 'GK Locations',
  'single' => TRUE,
);

function gk_locations_gk_locations_map_content_type_render($subtype, $conf, $args, $context) {
  if (($location = gk_locations_get_current_location()) && $geo = field_get_items('node', $location, 'field_location_geo')) {
    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => gk_locations_build_map(array(
        'center' => array(
          'geo' => array(
            'latitude' => $geo[0]['lat'],
            'longitude' => $geo[0]['lon'],
          ),
          'info_window' => $location->title,
        ),
        'zoom' => intval($conf['zoom']),
        'width' => intval($conf['width']),
        'height' => intval($conf['height']),
        'scrollwheel' => intval($conf['scrollwheel']),
      )),
    );
  }
}

function gk_locations_gk_locations_map_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['zoom'] = array(
    '#title' => t('Zoom'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(range(1, 20)),
    '#default_value' => isset($conf['zoom']) ? $conf['zoom'] : 12,
    '#required' => TRUE,
  );

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => isset($conf['width']) ? $conf['width'] : NULL,
  );

  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => isset($conf['height']) ? $conf['height'] : 350,
  );

  $form['scrollwheel'] = array(
    '#title' => t('Allow scrollwheel zoom?'),
    '#type' => 'checkbox',
    '#default_value' => isset($conf['scrollwheel']) ? $conf['scrollwheel'] : 0,
  );

  return $form;
}

function gk_locations_gk_locations_map_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
