<?php

$plugin = array(
  'title' => 'Locations: Description',
  'category' => 'GK Locations',
  'single' => TRUE,
);

function gk_locations_gk_locations_description_content_type_render($subtype, $conf, $args, $context) {
  if (($location = gk_locations_get_current_location()) && $description = field_get_items('node', $location, 'body')) {
    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => array(
        '#markup' => check_markup($description[0]['value'], $description[0]['format']),
      ),
    );
  }
}

function gk_locations_gk_locations_description_content_type_edit_form($form, &$form_state) {
  return $form;
}
