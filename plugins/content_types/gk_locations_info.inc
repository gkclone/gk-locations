<?php

$plugin = array(
  'title' => 'Locations: Info',
  'category' => 'GK Locations',
  'single' => TRUE,
);

function gk_locations_gk_locations_info_content_type_admin_title($subtype, $conf, $context = NULL) {
  $sections = array();
  $section_titles = gk_locations_info_content_type__sections();

  foreach ($conf['sections'] as $key => $section) {
    if ($section['display']) {
      $sections[] = $section_titles[$key];
    }
  }

  return 'Locations: Info (' . implode(', ', $sections) . ')';
}

function gk_locations_gk_locations_info_content_type_render($subtype, $conf, $args, $context) {
  if (empty($conf['location_nid']) || !$location = node_load($conf['location_nid'])) {
    $location = gk_locations_get_current_location();
  }

  // If a location node couldn't be determined then get out of here!
  if (!$location) {
    return array();
  }

  $content = array();

  // Render the introduction.
  if (!empty($conf['intro']['value'])) {
    $content['intro'] = array(
      '#theme_wrappers' => array('container'),
      '#attributes' => array(
        'class' => array(
          'LocationInfo',
          'LocationInfo--intro',
        ),
      ),
      'content' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array(
            'LocationInfo-content',
          ),
        ),
        '#markup' => check_markup($conf['intro']['value'], $conf['intro']['format']),
      ),
    );
  }

  // Get all checked sections.
  $active_sections = array();

  foreach ($conf['sections'] as $key => $section) {
    if ($section['display']) {
      $active_sections[$key] = $section;
    }
  }

  // If we're outputting multiple sections then show a title for each one.
  count($active_sections) > 1 && $section_titles = gk_locations_info_content_type__sections();

  // Render the sections.
  foreach ($active_sections as $key => $section) {
    $function = 'gk_locations_gk_locations_info_content_type_render__section_' . $key;

    if (function_exists($function) && $rendered_section = $function($location, $conf['sections'][$key])) {
      $content[$key] = array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array(
            'LocationInfo',
            'LocationInfo--' . minima_camelcase($key),
          ),
        ),
        'content' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array(
              'LocationInfo-content',
            ),
          ),
          'content' => $rendered_section,
        ),
        '#weight' => $section['weight'],
      );

      $section_title = NULL;

      if ($section['override_title']) {
        $section_title = $section['override_title_text'];
      }
      elseif (isset($section_titles[$key])) {
        $section_title = $section_titles[$key];
      }

      if ($section_title) {
        $content[$key]['title'] = array(
          '#theme' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $section_title,
          '#attributes' => array(
            'class' => array(
              'LocationInfo-title',
            ),
          ),
          '#weight' => -10,
        );
      }
    }
  }

  if (!empty($content)) {
    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : 'Information',
      'content' => $content,
    );
  }
}

function gk_locations_gk_locations_info_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Optional introduction text.
  $intro = isset($conf['intro']) ? $conf['intro'] : NULL;

  $form['intro'] = array(
    '#type' => 'text_format',
    '#title' => 'Intro',
    '#default_value' => $intro ? $intro['value'] : '',
    '#format' => $intro ? $intro['format'] : NULL,
  );

  // Create a fieldset for each available location info section.
  $form['sections'] = array(
    '#type' => 'fieldset',
    '#title' => 'Sections',
    '#tree' => TRUE,
  );

  $sections = isset($conf['sections']) ? $conf['sections'] : NULL;

  foreach (gk_locations_info_content_type__sections() as $name => $title) {
    $section = isset($sections[$name]) ? $sections[$name] : NULL;

    $form['sections'][$name] = array(
      '#tree' => TRUE,
      '#title' => $title,
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['sections'][$name]['display'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display?'),
      '#default_value' => $section ? $section['display'] : 0,
    );

    $form['sections'][$name]['override_title'] = array(
      '#type' => 'checkbox',
      '#title' => 'Override title',
      '#return_value' => TRUE,
      '#default_value' => $section ? $section['override_title'] : 0,
      '#prefix' => '<div class="option-text-aligner clearfix">',
    );

    $form['sections'][$name]['override_title_text'] = array(
      '#type' => 'textfield',
      '#default_value' => $section ? $section['override_title_text'] : '',
      '#suffix' => '</div>',
    );

    $form['sections'][$name]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#default_value' => $section ? $section['weight'] : 0,
    );

    // Make it possible to alter each section fieldset individually.
    $section_form_alter_function = 'gk_locations_gk_locations_info_content_type_edit_form_alter__section__' . $name;

    if (function_exists($section_form_alter_function)) {
      $section_form_alter_function($form['sections'][$name], $section);
    }
  }

  return $form;
}

function gk_locations_gk_locations_info_content_type_edit_form_validate($form, &$form_state) {
  foreach ($form_state['values']['sections'] as $section) {
    if ($section['display']) {
      return;
    }
  }

  form_set_error('', t('You must select one or more sections to display.'));
}

function gk_locations_gk_locations_info_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
  $form_state['conf']['sections'] = array_filter($form_state['conf']['sections']);
}

/**
 * Section render callbacks ====================================================
 */

// Contact.
function gk_locations_gk_locations_info_content_type_render__section_contact_details($location) {
  $content = array();

  foreach (array('telephone', 'email') as $name) {
    $field_name = 'field_location_' . $name;

    if ($items = field_get_items('node', $location, $field_name)) {
      if ($field_value = field_view_value('node', $location, $field_name, $items[0])) {
        $field_instance = field_info_instance('node', $field_name, 'location');

        $content[$name] = array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array(
              'LocationInfo-' . $name,
            ),
          ),
          'content' => array(
            'label' => array(
              '#theme_wrappers' => array('container'),
              '#attributes' => array(
                'class' => array(
                  'LocationInfo-label',
                ),
              ),
              'label' => array(
                '#markup' => $field_instance['label'],
              ),
            ),
            'value' => array(
              '#theme_wrappers' => array('container'),
              '#attributes' => array(
                'class' => array(
                  'LocationInfo-value',
                ),
              ),
              'value' => $field_value,
            ),
          ),
        );
      }
    }
  }

  return $content;
}

// Address.
function gk_locations_gk_locations_info_content_type_render__section_address($location, $conf) {
  if ($field = field_get_items('node', $location, 'field_location_address')) {
    $content = array(
      'address' => field_view_value('node', $location, 'field_location_address', $field[0]),
    );

    if ($conf['view_map_link']) {
      // Compile the map query.
      $address_fields = array(
        'thoroughfare',
        'locality',
        'administrative_area',
        'postal_code',
        'country',
      );

      $address_field_values = array();

      if ($site_name = variable_get('site_name')) {
        $address_field_values[] = urlencode($site_name);
      }

      foreach ($address_fields as $address_field) {
        $address_field_values[] = urlencode($content['address']['#address'][$address_field]);
      }

      $query = implode('+', $address_field_values);

      // Add the render array to $content
      $content['view_map_link'] = array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array('LocationInfo-cta'),
        ),
        'content' => array(
          '#type' => 'link',
          '#href' => 'http://maps.google.com/maps?z=17&q=' . $query,
          '#title' => t('View a map'),
        ),
      );
    }

    return $content;
  }
}

function gk_locations_gk_locations_info_content_type_edit_form_alter__section__address(&$form_section, $conf) {
  // Add the view a map checkbox to show the link to google maps.
  $form_section['view_map_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show \'View a map\' link?'),
    '#default_value' => $conf ? $conf['view_map_link'] : 0,
  );

  return $form_section;
}

// Opening hours.
function gk_locations_gk_locations_info_content_type_render__section_opening_hours($location) {
  $field_instance = field_info_instance('node', 'field_location_opening_hours', 'location');
  $field_display = field_get_display($field_instance, 'full', $location);
  $field_display['label'] = 'hidden';

  return field_view_field('node', $location, 'field_location_opening_hours', $field_display);
}

// Food hours.
function gk_locations_gk_locations_info_content_type_render__section_food_hours($location) {
  $field_instance = field_info_instance('node', 'field_location_food_hours', 'location');
  $field_display = field_get_display($field_instance, 'full', $location);
  $field_display['label'] = 'hidden';

  return field_view_field('node', $location, 'field_location_food_hours', $field_display);
}

// Facilities.
function gk_locations_gk_locations_info_content_type_render__section_facilities($location) {
  if ($facilities = field_get_items('node', $location, 'field_location_facilities')) {
    $tids = array();

    foreach ($facilities as $value) {
      $tids[] = $value['tid'];
    }

    if ($terms = taxonomy_term_load_multiple($tids)) {
      $items = array();

      foreach ($terms as $term) {
        $items[] = array(
          'data' => $term->name,
          'class' => array(
            drupal_html_class($term->name),
          ),
        );
      }

      if (!empty($items)) {
        return array(
          '#theme' => 'item_list',
          '#items' => $items,
        );
      }
    }
  }
}

/**
 * Return a list of available sections for the pane.
 */
function gk_locations_info_content_type__sections() {
  $sections = array(
    'contact_details' => 'Contact details',
    'address' => 'Address',
    'opening_hours' => 'Opening hours',
    'food_hours' => 'Food hours',
    'facilities' => 'Facilities',
  );

  drupal_alter('gk_locations_info_content_type__sections', $sections);
  return $sections;
}
