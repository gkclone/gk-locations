<?php

$plugin = array(
  'title' => 'Locations: Choose Form',
  'category' => 'GK Locations',
  'single' => TRUE,
);

function gk_locations_gk_locations_choose_form_content_type_render($subtype, $conf, $args, $context) {
  return (object) array(
    'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
    'content' => drupal_get_form('gk_locations_gk_locations_choose_form'),
  );
}

function gk_locations_gk_locations_choose_form_content_type_edit_form($form, &$form_state) {
  return $form;
}

/**
 * A form with a dropdown of locations.
 */
function gk_locations_gk_locations_choose_form($form, &$form_state) {
  $default_location = NULL;
  if (isset($form_state['gk_locations']['default_location'])) {
    $default_location = $form_state['gk_locations']['default_location'];
  }

  $first_option_text = 'Choose a location';
  if (isset($form_state['gk_locations']['first_option_text'])) {
    $first_option_text = $form_state['gk_locations']['first_option_text'];
  }

  $get_items_options = array();
  if (isset($form_state['gk_locations']['get_items_options'])) {
    $get_items_options = $form_state['gk_locations']['get_items_options'];
  }

  $options = array(
    0 => t($first_option_text),
  );

  foreach (gk_locations_get_items($get_items_options) as $location) {
    $options[$location->nid] = t($location->title);
  }

  $form['location'] = array(
    '#title' => t('Location'),
    '#title_display' => 'invisible',
    '#type' => 'select',
    '#options' => $options,
  );

  if (isset($default_location) && isset($options[$default_location])) {
    $form['location']['#default_value'] = $default_location;
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['#attributes']['class'] = array('gk-locations-choose-form');

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'gk_locations') . '/theme/js/gk_locations.choose-form.js',
  );

  return $form;
}

/**
 * Submit handler for the locations dropdown form.
 */
function gk_locations_gk_locations_choose_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['location'])) {
    $nid = $form_state['values']['location'];
    gk_locations_set_current_location_cookie($nid);
    $form_state['redirect'] = 'node/' . $nid;
  }
}
