<?php

$plugin = array(
  'title' => 'Locations: Search Form',
  'category' => 'GK Locations',
  'single' => TRUE,
);

function gk_locations_gk_locations_search_form_content_type_render($subtype, $conf, $args, $context) {
  return (object) array(
    'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
    'content' => drupal_get_form('gk_locations_gk_locations_search_form', $conf['show_advanced_filters']),
  );
}

function gk_locations_gk_locations_search_form_content_type_edit_form($form, &$form_state) {
  $show_advanced_filters = 0;

  if (isset($form_state['conf']['show_advanced_filters'])) {
    $show_advanced_filters = $form_state['conf']['show_advanced_filters'];
  }

  $form['show_advanced_filters'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show advanced filters',
    '#default_value' => $show_advanced_filters,
  );

  return $form;
}

function gk_locations_gk_locations_search_form_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}

/**
 * A form for searching for locations.
 */
function gk_locations_gk_locations_search_form($form, &$form_state, $show_advanced_filters = FALSE) {
  $form['location'] = array(
    '#type' => 'textfield',
    '#title' => t('Find a location'),
    '#attributes' => array(
      'placeholder' => t('Enter your location'),
    ),
    '#default_value' => !empty($_GET['location']) ? $_GET['location'] : '',
  );

  if ($show_advanced_filters) {
    $form['radius'] = array(
      '#type' => 'select',
      '#title' => t('Radius'),
      '#options' => drupal_map_assoc(array(1, 5, 10, 20, 50)),
      '#default_value' => !empty($_GET['radius']) ? $_GET['radius'] : NULL,
    );

    $facilities = array();
    $facilities_vocab = taxonomy_vocabulary_machine_name_load('facilities');

    foreach (taxonomy_get_tree($facilities_vocab->vid) as $term) {
      $facilities[$term->tid] = t($term->name);
    }

    if (!empty($facilities)) {
      $form['facilities'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Facilities'),
        '#options' => $facilities,
        '#default_value' => !empty($_GET['facilities']) ? $_GET['facilities'] : array(),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

/**
 * Validate handler for the locations search form.
 */
function gk_locations_gk_locations_search_form_validate($form, &$form_state) {
  // Prevent submission of the form if the location field is empty.
  $location = trim($form_state['values']['location']);

  if (empty($location)) {
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Submit handler for the locations search form.
 */
function gk_locations_gk_locations_search_form_submit($form, &$form_state) {
  // Lookup the node ID for the location search page.
  if ($uri = gk_machine_name_lookup_uri('gk_locations_search')) {
    // Remove values that we don't care about from the form state and redirect.
    form_state_values_clean($form_state);

    // Streamline the selection of facilities.
    if (!empty($form_state['values']['facilities'])) {
      $form_state['values']['facilities'] = array_filter($form_state['values']['facilities']);
    }

    $form_state['redirect'] = array($uri['path'], array(
      'query' => array_filter($form_state['values']),
    ));
  }
}
