<?php

$plugin = array(
  'title' => 'Locations: Banner',
  'category' => 'GK Locations',
  'single' => TRUE,
);

function gk_locations_gk_locations_banner_content_type_render($subtype, $conf, $args, $context) {
  if (($location = gk_locations_get_current_location()) && $items = field_get_items('node', $location, 'field_location_images')) {
    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => array(
        '#theme' => 'gk_core_slideshow',
        '#items' => $items,
        '#settings' => $conf,
      ),
    );
  }
}

function gk_locations_gk_locations_banner_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['timeout'] = array(
    '#title' => 'Timeout (ms)',
    '#type' => 'textfield',
    '#size' => 20,
    '#default_value' => isset($conf['timeout']) ? $conf['timeout'] : '4000',
    '#element_validate' => array('element_validate_integer_positive'),
    '#required' => TRUE,
  );

  $form['effect'] = array(
    '#title' => 'Effect',
    '#type' => 'select',
    '#default_value' => isset($conf['effect']) ? $conf['effect'] : 'fade',
    '#options' => gk_core_slideshow_effects(),
  );

  $form['image_style'] = array(
    '#title' => 'Image style',
    '#type' => 'select',
    '#default_value' => isset($conf['image_style']) ? $conf['image_style'] : 'gk_core_slideshow',
    '#empty_option' => 'None (original image)',
    '#options' => image_style_options(FALSE),
  );

  return $form;
}

function gk_locations_gk_locations_banner_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
