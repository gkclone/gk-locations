<?php

$plugin = array(
  'title' => 'Locations: List',
  'category' => 'GK Locations',
  'single' => TRUE,
);

function gk_locations_gk_locations_list_content_type_render($subtype, $conf, $args, $context) {
  // Determine how we're ordering. Order by node title by default.
  $order = 'letter';

  if (!empty($_GET['order']) && in_array($_GET['order'], array('location', 'letter'))) {
    $order = $_GET['order'];
  }

  // Get the locations ordered by title (default) or location.
  $options = array();

  if ($order == 'location') {
    $options['order']['field'][] = array(
      'name' => 'field_location_address',
      'column' => 'administrative_area',
    );
    $options['order']['field'][] = array(
      'name' => 'field_location_address',
      'column' => 'locality',
    );
    $options['order']['property'][] = array(
      'name' => 'title',
    );
  }

  if ($locations = gk_locations_get_items($options)) {
    // Split the locations into groups based on $order. The duplicated loops
    // here are ugly but they're more efficient than checking the value of
    // $order on each iteration.
    $grouped_locations = array();

    switch ($order) {
      case 'location':
        foreach ($locations as $nid => $location) {
          $address = field_get_items('node', $location, 'field_location_address');
          $group_title = $address[0]['administrative_area'];
          $grouped_locations[$group_title][$nid] = $location;
        }
      break;

      case 'letter':
        foreach ($locations as $nid => $location) {
          $group_title = strtoupper($location->title[0]);
          $grouped_locations[$group_title][$nid] = $location;
        }
      break;
    }

    if (!empty($grouped_locations)) {
      // Turn each group of locations into a renderable array.
      $theme = 'gk_locations_list_' . $order . '_group';

      foreach ($grouped_locations as $group_title => &$group) {
        $locations = $group;

        $group = array(
          '#theme' => $theme,
          '#group_title' => $group_title,
          '#locations' => $locations,
        );
      }

      // Build an array of classes for use with the order-by links.
      $opposites = array(
        'location' => 'letter',
        'letter' => 'location',
      );
      $order_by_link_class = array();
      $order_by_link_class[$order] = 'is-active';
      $order_by_link_class[$opposites[$order]] = '';

      // Piece together the final renderable array.
      return (object) array(
        'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
        'content' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array(
              'LocationList',
              'LocationList--by' . ucfirst($order),
              'Grid',
            ),
          ),
          'content' => array(
            'order_by_links' => array(
              '#theme_wrappers' => array('container'),
              '#attributes' => array(
                'class' => array(
                  'Box',
                  'Box--orderByLinks',
                  'Grid-cell',
                ),
              ),
              'inner' => array(
                '#theme_wrappers' => array('container'),
                '#attributes' => array(
                  'class' => array('Box-inner'),
                ),
                'content' => array(
                  '#theme_wrappers' => array('container'),
                  '#attributes' => array(
                    'class' => array('Box-content'),
                  ),
                  'by_location' => array(
                    '#theme' => 'link',
                    '#text' => t('Location'),
                    '#path' => current_path(),
                    '#options' => array(
                      'html' => FALSE,
                      'attributes' => array(
                        'class' => array($order_by_link_class['location']),
                      ),
                      'query' => array(
                        'order' => 'location',
                      ),
                    ),
                  ),
                  'by_letter' => array(
                    '#theme' => 'link',
                    '#text' => t('Pub name'),
                    '#path' => current_path(),
                    '#options' => array(
                      'html' => FALSE,
                      'attributes' => array(
                        'class' => array($order_by_link_class['letter']),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            'grouped_locations' => $grouped_locations,
          ),
        ),
      );
    }
  }
}

function gk_locations_gk_locations_list_content_type_edit_form($form, &$form_state) {
  return $form;
}
