<?php

$plugin = array(
  'title' => 'Locations: Search Results',
  'category' => 'GK Locations',
  'single' => TRUE,
);

function gk_locations_gk_locations_search_results_content_type_render($subtype, $conf, $args, $context) {
  if (!empty($_GET['location']) && $geocode_result = gk_locations_geocode($_GET['location'])) {
    // Only get the locations once.
    static $gk_locations_get_items__result = NULL;

    if (!isset($gk_locations_get_items__result)) {
      $get_items_options = array('filters' => $_GET);

      // Honour the limit in the URL.
      if (isset($_GET['limit'])) {
        $get_items_options['limit'] = $_GET['limit'];
      }
      // Otherwise, limit the results only if a radius is not specified.
      elseif (!isset($_GET['radius'])) {
        $get_items_options['limit'] = 5;
      }

      $gk_locations_get_items__result = gk_locations_get_items($get_items_options);
    }

    $items = $gk_locations_get_items__result;

    switch ($conf['type']) {
      case 'map':
        $markers = array();

        foreach ($items as $item) {
          $geo = field_get_items('node', $item, 'field_location_geo');

          $markers[] = array(
            'geo' => array(
              'latitude' => $geo[0]['lat'],
              'longitude' => $geo[0]['lon'],
            ),
            'info_window' => theme('gk_locations_search_results_map_infowindow', array(
              'location' => $item,
            )),
          );
        }

        $content = gk_locations_build_map(array(
          'center' => array(
            'geo' => array(
              'latitude' => $geocode_result['latitude'],
              'longitude' => $geocode_result['longitude'],
            ),
            'info_window' => ucfirst($_GET['location']),
          ),
          'markers' => $markers,
        ));
      break;

      case 'list':
        $locations = array();

        foreach ($items as $item) {
          $list_item = array(
            '#theme' => 'gk_locations_search_results_list_item',
            '#location' => $item,
          );

          if (isset($item->proximity_search)) {
            $distance = round($item->proximity_search['distance'], 1);
            $list_item['#distance'] = $distance . ' ' . $item->proximity_search['units'];
          }

          $locations[] = $list_item;
        }

        $content = array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('LocationSearchResults'),
          ),
          'locations' => $locations,
        );
      break;
    }

    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => $content,
    );
  }
}

function gk_locations_gk_locations_search_results_content_type_edit_form($form, &$form_state) {
  $form['type'] = array(
    '#type' => 'select',
    '#title' => 'Type',
    '#options' => array('list' => 'List', 'map' => 'Map'),
    '#default_value' => isset($form_state['conf']['type']) ? $form_state['conf']['type'] : 'list',
  );

  return $form;
}

function gk_locations_gk_locations_search_results_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
