(function($) {

Drupal.behaviors.gk_locations_choose_form = {
  attach: function (context, settings) {
    // Make the 'choose form' more user friendly.
    $('.gk-locations-choose-form').each(function() {
      // Submit the form when the selection is changed.
      $(this).find('select').change(function() {
        $(this).parents('form').submit();
      });

      // The submit button is no longer required...
      $(this).find('input[type=submit]').hide();
    });
  }
};

})(jQuery);
