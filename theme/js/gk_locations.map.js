(function($) {

Drupal.behaviors.gk_locations_map = {
  attach: function (context, settings) {
    if (typeof Drupal.settings.gk_locations.map === 'undefined') {
      return;
    }

    var options = Drupal.settings.gk_locations.map;

    $('.LocationMap').each(function(index, element) {
      // Set width/height of the map element.
      $(this).width(options.width || 'auto');
      $(this).height(options.height || 350);

      // Build options to pass to the maps API.
      var map_options = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoom: options.zoom || 12,
        mapTypeControl: false,
        center: new google.maps.LatLng(
          options.center.geo.latitude,
          options.center.geo.longitude
        ),
        scrollwheel: options.scrollwheel
      };

      var map = new google.maps.Map(element, map_options),
        info_window = new google.maps.InfoWindow(),

      // Function used to set the content and position of the above info window
      // on the above map when a user clicks a marker.
      placeInfoWindow = function(marker, content) {
        google.maps.event.addListener(marker, 'click', function() {
          info_window.setContent(content);
          info_window.open(map, marker);
        });
      },

      // Add a marker at the center.
      marker_options = {
        map: map,
        position: map_options.center
      };

      if (typeof options.marker_images !== 'undefined') {
        if (typeof options.marker_images.center !== 'undefined') {
          marker_options.icon = options.marker_images.center;
        }
      }

      var center_marker = new google.maps.Marker(marker_options);

      // Info window at the center?
      if (typeof options.center.info_window !== 'undefined') {
        placeInfoWindow(center_marker, options.center.info_window);
      }

      // If we've been given a set of markers then place these on the map too.
      if (typeof options.markers !== 'undefined') {
        var bounds = new google.maps.LatLngBounds();

        for (var i in options.markers) {
          // Build options for this marker.
          var lat_lon = new google.maps.LatLng(
            options.markers[i].geo.latitude,
            options.markers[i].geo.longitude
          ),
          location_marker_options = {
            map: map,
            position: lat_lon
          }

          if (typeof options.marker_images !== 'undefined') {
            if (typeof options.marker_images.location !== 'undefined') {
              location_marker_options.icon = options.marker_images.location;
            }
          }

          var location_marker = new google.maps.Marker(location_marker_options);

          // Does the marker define content for an info window?
          if (typeof options.markers[i].info_window !== 'undefined') {
            placeInfoWindow(location_marker, options.markers[i].info_window);
          }

          // Extend the bounds of the map to accommodate this marker.
          bounds.extend(lat_lon);
        }

        // Extend the bounds once more to accommodate for the map center and
        // apply the bounds to the map object.
        bounds.extend(map_options.center);
        map.fitBounds(bounds);
      }

      // Store the map object and options somewhere that other JS can access it.
      Drupal.settings.gk_locations.map.objMap = map;
      Drupal.settings.gk_locations.map.options = map_options;
    });
  }
};

})(jQuery);
