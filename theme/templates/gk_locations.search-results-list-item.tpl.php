<div class="LocationSearchResult">
  <div class="LocationSearchResult-title">
    <?php echo l($location->title, 'node/' . $location->nid); ?>
  </div>

  <?php if (isset($distance)): ?>
    <div class="LocationSearchResult-distance">
      <?php echo $distance; ?>
    </div>
  <?php endif; ?>
</div>
