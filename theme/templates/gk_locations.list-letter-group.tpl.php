<div class="Box Box--locationListGroup Grid-cell" id="letter-<?php echo $group_title_html_safe; ?>">
  <div class="Box-inner">
    <h2 class="Box-title">
      <?php echo $group_title; ?>
    </h2>

    <ul class="Box-content">
      <?php foreach ($location_links as $link): ?>
        <li>
          <?php echo render($link); ?>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
