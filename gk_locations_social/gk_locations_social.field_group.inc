<?php
/**
 * @file
 * gk_locations_social.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function gk_locations_social_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_location_social|node|location|form';
  $field_group->group_name = 'group_location_social';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Social media',
    'weight' => '13',
    'children' => array(
      0 => 'field_location_social_facebook',
      1 => 'field_location_social_twitter',
      2 => 'field_location_social_instagram',
      3 => 'field_location_social_tripadv',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Social media',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-location-social field-group-fieldset',
        'description' => 'Input social media profile handles only (e.g. PubName).',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_location_social|node|location|form'] = $field_group;

  return $export;
}
