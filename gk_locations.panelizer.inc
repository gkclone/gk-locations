<?php
/**
 * @file
 * gk_locations.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gk_locations_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:location:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'location';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '2_columns_75_25_content_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'bc5e2e41-9444-4b8c-8e85-0991717befe8';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b4d0f760-a530-4be9-a779-3e272429acc8';
    $pane->panel = 'content';
    $pane->type = 'gk_locations_banner';
    $pane->subtype = 'gk_locations_banner';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'timeout' => '4000',
      'effect' => 'fade',
      'image_style' => 'gk_locations',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b4d0f760-a530-4be9-a779-3e272429acc8';
    $display->content['new-b4d0f760-a530-4be9-a779-3e272429acc8'] = $pane;
    $display->panels['content'][0] = 'new-b4d0f760-a530-4be9-a779-3e272429acc8';
    $pane = new stdClass();
    $pane->pid = 'new-2ffc1ef2-8b69-48ba-9181-c66317450e3f';
    $pane->panel = 'content_bottom';
    $pane->type = 'gk_locations_description';
    $pane->subtype = 'gk_locations_description';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2ffc1ef2-8b69-48ba-9181-c66317450e3f';
    $display->content['new-2ffc1ef2-8b69-48ba-9181-c66317450e3f'] = $pane;
    $display->panels['content_bottom'][0] = 'new-2ffc1ef2-8b69-48ba-9181-c66317450e3f';
    $pane = new stdClass();
    $pane->pid = 'new-4aa1ea31-1664-4d4f-87ba-75787fc5b888';
    $pane->panel = 'content_bottom';
    $pane->type = 'gk_locations_map';
    $pane->subtype = 'gk_locations_map';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'zoom' => '12',
      'width' => '',
      'height' => '350',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4aa1ea31-1664-4d4f-87ba-75787fc5b888';
    $display->content['new-4aa1ea31-1664-4d4f-87ba-75787fc5b888'] = $pane;
    $display->panels['content_bottom'][1] = 'new-4aa1ea31-1664-4d4f-87ba-75787fc5b888';
    $pane = new stdClass();
    $pane->pid = 'new-605ef491-498e-45ab-a9de-e0ed106331cd';
    $pane->panel = 'content_top';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '605ef491-498e-45ab-a9de-e0ed106331cd';
    $display->content['new-605ef491-498e-45ab-a9de-e0ed106331cd'] = $pane;
    $display->panels['content_top'][0] = 'new-605ef491-498e-45ab-a9de-e0ed106331cd';
    $pane = new stdClass();
    $pane->pid = 'new-1460abfb-b5c8-47d1-800b-6026d551b545';
    $pane->panel = 'secondary';
    $pane->type = 'gk_locations_info';
    $pane->subtype = 'gk_locations_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Contact details',
      'intro' => array(
        'value' => '',
        'format' => 'full_html',
      ),
      'sections' => array(
        'contact_details' => array(
          'display' => 1,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'address' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
          'view_map_link' => 0,
        ),
        'opening_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'food_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'facilities' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'social' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1460abfb-b5c8-47d1-800b-6026d551b545';
    $display->content['new-1460abfb-b5c8-47d1-800b-6026d551b545'] = $pane;
    $display->panels['secondary'][0] = 'new-1460abfb-b5c8-47d1-800b-6026d551b545';
    $pane = new stdClass();
    $pane->pid = 'new-ec0b5688-9892-4813-b578-5e4f7cf8d289';
    $pane->panel = 'secondary';
    $pane->type = 'gk_locations_info';
    $pane->subtype = 'gk_locations_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Opening hours',
      'intro' => array(
        'value' => '',
        'format' => 'full_html',
      ),
      'sections' => array(
        'contact_details' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'address' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
          'view_map_link' => 0,
        ),
        'opening_hours' => array(
          'display' => 1,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'food_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'facilities' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'social' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ec0b5688-9892-4813-b578-5e4f7cf8d289';
    $display->content['new-ec0b5688-9892-4813-b578-5e4f7cf8d289'] = $pane;
    $display->panels['secondary'][1] = 'new-ec0b5688-9892-4813-b578-5e4f7cf8d289';
    $pane = new stdClass();
    $pane->pid = 'new-00e8a1ba-422b-408a-ae52-12b11f922f9e';
    $pane->panel = 'secondary';
    $pane->type = 'gk_locations_info';
    $pane->subtype = 'gk_locations_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Facilities',
      'intro' => array(
        'value' => '',
        'format' => 'full_html',
      ),
      'sections' => array(
        'contact_details' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'address' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
          'view_map_link' => 0,
        ),
        'opening_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'food_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'facilities' => array(
          'display' => 1,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'social' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '00e8a1ba-422b-408a-ae52-12b11f922f9e';
    $display->content['new-00e8a1ba-422b-408a-ae52-12b11f922f9e'] = $pane;
    $display->panels['secondary'][2] = 'new-00e8a1ba-422b-408a-ae52-12b11f922f9e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:location:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_locations_main';
  $panelizer->title = 'Locations: Main';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '2_columns_75_25';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'dcdc12de-71c1-4249-943b-7012cc05ddd6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c073a08d-7be8-41ba-ab06-143cb77f5060';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c073a08d-7be8-41ba-ab06-143cb77f5060';
    $display->content['new-c073a08d-7be8-41ba-ab06-143cb77f5060'] = $pane;
    $display->panels['primary'][0] = 'new-c073a08d-7be8-41ba-ab06-143cb77f5060';
    $pane = new stdClass();
    $pane->pid = 'new-c592285b-d75e-4f7b-bae9-b6cdd5177378';
    $pane->panel = 'primary';
    $pane->type = 'gk_locations_search_form';
    $pane->subtype = 'gk_locations_search_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'show_advanced_filters' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c592285b-d75e-4f7b-bae9-b6cdd5177378';
    $display->content['new-c592285b-d75e-4f7b-bae9-b6cdd5177378'] = $pane;
    $display->panels['primary'][1] = 'new-c592285b-d75e-4f7b-bae9-b6cdd5177378';
    $pane = new stdClass();
    $pane->pid = 'new-c07c8279-0e55-4892-81da-53402c09513e';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c07c8279-0e55-4892-81da-53402c09513e';
    $display->content['new-c07c8279-0e55-4892-81da-53402c09513e'] = $pane;
    $display->panels['primary'][2] = 'new-c07c8279-0e55-4892-81da-53402c09513e';
    $pane = new stdClass();
    $pane->pid = 'new-aecaf51b-161a-43ac-820c-6c9dd2d216b4';
    $pane->panel = 'primary';
    $pane->type = 'gk_locations_list';
    $pane->subtype = 'gk_locations_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'aecaf51b-161a-43ac-820c-6c9dd2d216b4';
    $display->content['new-aecaf51b-161a-43ac-820c-6c9dd2d216b4'] = $pane;
    $display->panels['primary'][3] = 'new-aecaf51b-161a-43ac-820c-6c9dd2d216b4';
    $pane = new stdClass();
    $pane->pid = 'new-f7cf84c0-699f-4496-b870-04c23611be03';
    $pane->panel = 'secondary';
    $pane->type = 'gk_locations_info';
    $pane->subtype = 'gk_locations_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'intro' => array(
        'value' => '',
        'format' => 'full_html',
      ),
      'sections' => array(
        'contact_details' => array(
          'display' => 1,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'address' => array(
          'display' => 1,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '1',
          'view_map_link' => 0,
        ),
        'opening_hours' => array(
          'display' => 1,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '2',
        ),
        'food_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'facilities' => array(
          'display' => 1,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '3',
        ),
        'social' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f7cf84c0-699f-4496-b870-04c23611be03';
    $display->content['new-f7cf84c0-699f-4496-b870-04c23611be03'] = $pane;
    $display->panels['secondary'][0] = 'new-f7cf84c0-699f-4496-b870-04c23611be03';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_locations_main'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_locations_search';
  $panelizer->title = 'Locations: Search';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '6d4e4ffe-2dab-47d8-81ac-147544ce532d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9a9f14e0-8d01-4e39-ae34-ea8c31d7aa84';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9a9f14e0-8d01-4e39-ae34-ea8c31d7aa84';
    $display->content['new-9a9f14e0-8d01-4e39-ae34-ea8c31d7aa84'] = $pane;
    $display->panels['primary'][0] = 'new-9a9f14e0-8d01-4e39-ae34-ea8c31d7aa84';
    $pane = new stdClass();
    $pane->pid = 'new-d2f3466b-81de-44c3-8c96-e3d1db1d0523';
    $pane->panel = 'primary';
    $pane->type = 'gk_locations_search_form';
    $pane->subtype = 'gk_locations_search_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'show_advanced_filters' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'd2f3466b-81de-44c3-8c96-e3d1db1d0523';
    $display->content['new-d2f3466b-81de-44c3-8c96-e3d1db1d0523'] = $pane;
    $display->panels['primary'][1] = 'new-d2f3466b-81de-44c3-8c96-e3d1db1d0523';
    $pane = new stdClass();
    $pane->pid = 'new-75b89e9b-431d-4dee-9c95-04c111bfbf65';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '75b89e9b-431d-4dee-9c95-04c111bfbf65';
    $display->content['new-75b89e9b-431d-4dee-9c95-04c111bfbf65'] = $pane;
    $display->panels['primary'][2] = 'new-75b89e9b-431d-4dee-9c95-04c111bfbf65';
    $pane = new stdClass();
    $pane->pid = 'new-4f6d4e71-3d12-4138-bb66-83084bb48dd8';
    $pane->panel = 'primary';
    $pane->type = 'gk_locations_search_results';
    $pane->subtype = 'gk_locations_search_results';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'type' => 'map',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '4f6d4e71-3d12-4138-bb66-83084bb48dd8';
    $display->content['new-4f6d4e71-3d12-4138-bb66-83084bb48dd8'] = $pane;
    $display->panels['primary'][3] = 'new-4f6d4e71-3d12-4138-bb66-83084bb48dd8';
    $pane = new stdClass();
    $pane->pid = 'new-71c472dc-2751-47b6-b826-aaea7c9c0a34';
    $pane->panel = 'primary';
    $pane->type = 'gk_locations_search_results';
    $pane->subtype = 'gk_locations_search_results';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'type' => 'list',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '71c472dc-2751-47b6-b826-aaea7c9c0a34';
    $display->content['new-71c472dc-2751-47b6-b826-aaea7c9c0a34'] = $pane;
    $display->panels['primary'][4] = 'new-71c472dc-2751-47b6-b826-aaea7c9c0a34';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_locations_search'] = $panelizer;

  return $export;
}
