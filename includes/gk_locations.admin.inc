<?php

/**
 * Settings form for locations.
 */
function gk_locations_settings_form() {
  $file_fids = variable_get('gk_locations_default_image_fids', '');

  if (is_array($file_fids)) {
    $file_fids = implode(',', $file_fids);
  }

  $form['gk_locations_default_image_fids'] = array(
    '#type' => 'textfield',
    '#title' => t('Default image file ids (fids)'),
    '#default_value' => $file_fids,
    '#description' => t('A comma seperated list of File IDs (fids). e.g. 21,54,92,32'),
  );

  return system_settings_form($form);
}

/**
 * Validate handler for the locations settings form.
 */
function gk_locations_settings_form_validate($form, &$form_state) {
  if (    !empty($form_state['values']['gk_locations_default_image_fids'])
       && $fids = explode(',', $form_state['values']['gk_locations_default_image_fids'])
    ) {
    foreach ($fids as &$fid) {
      $fid = trim($fid);

      if (!is_numeric($fid)) {
        form_set_error(
          'gk_locations_default_image_fids',
          t('%fid is not numeric. Please enter a list of comma seperated File IDs.', array(
            '%fid' => "'$fid'",
          ))
        );
      }
    }

    $form_state['values']['gk_locations_default_image_fids'] = $fids;
  }
}
