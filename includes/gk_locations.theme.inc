<?php

/**
 * Preprocess variables for theme_minima_layout().
 */
function gk_locations_preprocess_minima_page_layout(&$variables) {
  // The locations list page uses the 72/25 2-column layout and the location
  // info block is placed in the secondary region by default. If the user has
  // not visited a location node page yet then the region would be empty, so
  // we need to remove the region if that's the case.
  if ($variables['layout']['name'] == '2_columns_75_25') {
    if (arg(0) == 'node' && gk_machine_name_lookup_by_entity(arg(1)) == 'gk_locations_main') {
      if (gk_locations_get_current_location(FALSE) === NULL) {
        unset($variables['minima_layout']['main']['secondary']);
        $classes = &$variables['minima_layout']['main']['primary']['#grid_cell_attributes']['class'];

        if (($key = array_search('u-xl-size3of4', $classes)) !== FALSE) {
          unset($classes[$key]);
        }
      }
    }
  }
}

/**
 * Preprocess variables for theme_panels_pane().
 */
function gk_locations_preprocess_panels_pane(&$variables) {
  $pane = $variables['pane'];

  if ($pane->type == 'gk_locations_info') {
    $variables['attributes_array']['class'][] = 'Box--locationInfo';

    // Apply classes based on the sections configured to display.
    foreach ($pane->configuration['sections'] as $key => $section) {
      if ($section['display']) {
        $variables['attributes_array']['class'][] = 'Box--locationInfo' . minima_camelcase($key, '[-|_]+', TRUE);
      }
    }
  }
  elseif ($pane->type == 'gk_locations_map') {
    $variables['attributes_array']['class'][] = 'Box--locationMap';
  }
}

/**
 * Preprocess function for the gk_locations.list-*-group templates.
 */
function template_preprocess_gk_locations_list_group(&$variables) {
  $group = $variables['group'];

  $variables['group_title'] = $group['#group_title'];
  $variables['group_title_html_safe'] = drupal_html_id($group['#group_title']);

  // Build an array of links for the locations in this group.
  $variables['location_links'] = array();

  foreach ($group['#locations'] as $location) {
    $variables['location_links'][$location->nid] = array(
      '#theme' => 'link',
      '#text' => $location->title,
      '#path' => 'node/' . $location->nid,
      '#options' => array(
        'attributes' => array(),
        'html' => FALSE,
      ),
    );
  }
}

/**
 * Preprocess function for the gk_locations.list-location-group template.
 */
function template_preprocess_gk_locations_list_location_group(&$variables) {
  // Further group the locations by locality.
  $variables['localities'] = array();

  foreach ($variables['group']['#locations'] as $location) {
    $address = field_get_items('node', $location, 'field_location_address');

    $locality_title = $address[0]['locality'];
    $variables['localities'][$locality_title][] = $location;

    if (!isset($variables['locality_titles_html_safe'][$locality_title])) {
      $variables['locality_titles_html_safe'][$locality_title] = drupal_html_id($locality_title);
    }
  }
}

/**
 * Preprocess function for the gk_locations.list-letter-group template.
 */
function template_preprocess_gk_locations_list_letter_group(&$variables) {
  // Add the locality part of the address to each locations name.
  foreach ($variables['group']['#locations'] as $location) {
    $address = field_get_items('node', $location, 'field_location_address');

    if (!empty($address[0]['locality'])) {
      $variables['location_links'][$location->nid]['#text'] .= ', ' . $address[0]['locality'];
    }
  }
}
